from prometheus_client import start_http_server, Summary
import asyncio
import httpx
import json
import math

# Define metrics.
metric_names = [
    # Air quality
    'normalized_pm1_0', 'normalized_pm2_5', 'normalized_pm10',
    'pm1_0', 'pm2_5', 'pm10',

    'particles_greater_than_0_3um',
    'particles_greater_than_0_5um',
    'particles_greater_than_1_0um',
    'particles_greater_than_2_5um',
    'particles_greater_than_5_0um',
    'particles_greater_than_10um',
]
metrics = { m : Summary('home_' + m, m) for m in metric_names }
metrics['temperature'] = Summary('home_temperature_celsius', 'temperature_celsius', labelnames=['channel'])
metrics['humidity'] = Summary('home_humidity_percentrh', 'humidity_percentrh', labelnames=['channel'])
metrics['abs_humidity'] = Summary('home_humidity_g_per_Nm3', 'humidity_g_per_Nm3', labelnames=['channel'])
metrics['outdoor_temperature'] = Summary('home_outdoor_temperature_celsius', 'outdoor_temperature_celsius')
metrics['outdoor_humidity'] = Summary('home_outdoor_humidity_percentrh', 'outdoor_humidity_percentrh')
metrics['outdoor_pressure'] = Summary('home_outdoor_pressure_hPa', 'outdoor_pressure_hPa')
metrics['outdoor_abs_humidity'] = Summary('home_outdoor_humidity_g_per_Nm3', 'outdoor_humidity_g_per_Nm3')

def log_aq(line):
    # Log prometheus summary for each element in the json
    data = json.loads(line)
    for k, v in data.items():
        metrics[k].observe(v)

last_outdoor_pressure_hPa = 1013.25  # assume 1atm if no data
def log_temp_hum(line):
    # Log prometheus summary from the json for each key in the list
    data = json.loads(line)
    channel = data['channel']
    temp_c = data['temperature_C']
    hum_percentrh = data['humidity']
    metrics['temperature'].labels(channel).observe(temp_c)
    metrics['humidity'].labels(channel).observe(hum_percentrh)
    abs_hum = absolute_hum_g_per_Nm3(temp_c, hum_percentrh, last_outdoor_pressure_hPa)
    metrics['abs_humidity'].labels(channel).observe(abs_hum)

def saturated_vapor_pressure_Pa(temp_c):
    # August-Roche-Magnus equation for >0C @ 1atm.
    # (It varies weakly with pressure, so that's ignored.)
    return 610.94 * math.exp((17.625 * temp_c) / (temp_c + 243.04))

def g_per_m3_to_g_per_Nm3(g_per_m3, temp_c, pressure_hPa):
    temp_k = temp_c + 273.15
    pressure_Pa = pressure_hPa * 100
    # Nk = PV/T; take V=1
    Nk_actual = pressure_Pa / temp_k
    Nk_normal = 101325 / 288.15  # ISO 2533 at sea level
    return g_per_m3 * (Nk_normal / Nk_actual)

def absolute_hum_g_per_Nm3(temp_c, hum_percentrh, pressure_hPa):
    sat_vapor_pressure_Pa = saturated_vapor_pressure_Pa(temp_c)
    sat_vapor_density_g_per_m3 = sat_vapor_pressure_Pa / 0.4615 / (temp_c + 273.15)
    sat_vapor_density_g_per_Nm3 = g_per_m3_to_g_per_Nm3(sat_vapor_density_g_per_m3, temp_c, pressure_hPa)
    vapor_density_g_per_Nm3 = sat_vapor_density_g_per_Nm3 * hum_percentrh / 100
    return vapor_density_g_per_Nm3

async def log_outdoor_weather():
    key = '859b30ab3207edc2361ddac22ed690ff'
    url = 'https://api.openweathermap.org/data/2.5/weather?lat=37.568&lon=-122.319&units=metric&APPID=' + key
    j = (await httpx.get(url)).json()
    temp_c = j['main']['temp']
    hum_percentrh = j['main']['humidity']
    pressure_hPa = j['main']['pressure']
    last_outdoor_pressure_hPa = pressure_hPa

    metrics['outdoor_temperature'].observe(temp_c)
    metrics['outdoor_humidity'].observe(hum_percentrh)
    metrics['outdoor_pressure'].observe(pressure_hPa)
    abs_hum = absolute_hum_g_per_Nm3(temp_c, hum_percentrh, pressure_hPa)
    metrics['outdoor_abs_humidity'].observe(abs_hum)

async def run_outdoor_weather():
    while True:
        try:
            await log_outdoor_weather()
        except:
            pass
        await asyncio.sleep(60)


async def run_subprocess(callback, program, *args):
    proc = await asyncio.create_subprocess_exec(program, *args, stdout=asyncio.subprocess.PIPE)
    try:
        while True:
            callback(await proc.stdout.readline())
    finally:
        proc.kill()


loop = asyncio.get_event_loop()
try:
    # Start up the server to expose the metrics.
    start_http_server(8050)
    loop.create_task(run_subprocess(log_aq, 'env/bin/python3', 'airquality.py', '/dev/ttyS0'))
    #loop.create_task(run_subprocess(log_temp_hum, 'rtl_433', '-Mnewmodel', '-Fjson', '-R40'))
    #loop.create_task(run_outdoor_weather())
    loop.run_forever()
finally:
    loop.close()
