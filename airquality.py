import json
import serial
import struct
import sys
import RPi.GPIO as GPIO

class Sample():
    def __init__(self, values=[0,0,0,0,0,0,0,0,0,0,0,0]):
        self.normalized_pm1_0             = values[0]
        self.normalized_pm2_5             = values[1]
        self.normalized_pm10              = values[2]
        self.pm1_0                        = values[3]
        self.pm2_5                        = values[4]
        self.pm10                         = values[5]
        self.particles_greater_than_0_3um = values[6]
        self.particles_greater_than_0_5um = values[7]
        self.particles_greater_than_1_0um = values[8]
        self.particles_greater_than_2_5um = values[9]
        self.particles_greater_than_5_0um = values[10]
        self.particles_greater_than_10um  = values[11]
    def __iadd__(self, other):
        self.normalized_pm1_0             += other.normalized_pm1_0
        self.normalized_pm2_5             += other.normalized_pm2_5
        self.normalized_pm10              += other.normalized_pm10
        self.pm1_0                        += other.pm1_0
        self.pm2_5                        += other.pm2_5
        self.pm10                         += other.pm10
        self.particles_greater_than_0_3um += other.particles_greater_than_0_3um
        self.particles_greater_than_0_5um += other.particles_greater_than_0_5um
        self.particles_greater_than_1_0um += other.particles_greater_than_1_0um
        self.particles_greater_than_2_5um += other.particles_greater_than_2_5um
        self.particles_greater_than_5_0um += other.particles_greater_than_5_0um
        self.particles_greater_than_10um  += other.particles_greater_than_10um
        return self
    def __truediv__(self, other):
        return Sample([
                self.normalized_pm1_0 / other,
                self.normalized_pm2_5 / other,
                self.normalized_pm10 / other,
                self.pm1_0 / other,
                self.pm2_5 / other,
                self.pm10 / other,
                self.particles_greater_than_0_3um / other,
                self.particles_greater_than_0_5um / other,
                self.particles_greater_than_1_0um / other,
                self.particles_greater_than_2_5um / other,
                self.particles_greater_than_5_0um / other,
                self.particles_greater_than_10um / other,
            ])
    def as_obj(self):
        return {
            'normalized_pm1_0':             self.normalized_pm1_0,
            'normalized_pm2_5':             self.normalized_pm2_5,
            'normalized_pm10':              self.normalized_pm10,
            'pm1_0':                        self.pm1_0,
            'pm2_5':                        self.pm2_5,
            'pm10':                         self.pm10,
            'particles_greater_than_0_3um': self.particles_greater_than_0_3um,
            'particles_greater_than_0_5um': self.particles_greater_than_0_5um,
            'particles_greater_than_1_0um': self.particles_greater_than_1_0um,
            'particles_greater_than_2_5um': self.particles_greater_than_2_5um,
            'particles_greater_than_5_0um': self.particles_greater_than_5_0um,
            'particles_greater_than_10um':  self.particles_greater_than_10um,
        }

class SampleGathererPrinter():
    def __init__(self):
        self.json_enc = json.JSONEncoder(separators=(',', ':'))

    def sample(self, values):
        sample = Sample(values)
        print(self.json_enc.encode(sample.as_obj()))
        sys.stdout.flush()

if __name__ == '__main__':
    def usage_exit(msg):
        sys.stderr.write(msg + '\n')
        sys.stderr.write("Usage: {} /dev/ttyXXX\n".format(sys.argv[0]))
        sys.exit(0)

    if len(sys.argv) != 2:
        usage_exit('Not enough arguments')

    serial_port = sys.argv[1]

    # Configure the sensor (if the input pins are connected)
    GPIO.setmode(GPIO.BCM)
    # set the SET pin to "normal" (not sleeping)
    GPIO.setup(23, GPIO.OUT)
    GPIO.output(23, GPIO.HIGH)
    # set the RESET pin to "normal"
    GPIO.setup(24, GPIO.OUT)
    GPIO.output(24, GPIO.HIGH)

    uart = serial.Serial(serial_port, baudrate=9600, timeout=0.25)
    buffer = []

    sampler = SampleGathererPrinter()
    while True:
        data = uart.read(32)  # read up to 32 bytes
        buffer += list(data)

        while buffer and buffer[0] != 0x42:
            buffer.pop(0)

        if len(buffer) > 200:
            buffer = []  # avoid an overrun if all bad data
        if len(buffer) < 32:
            continue

        if buffer[1] != 0x4d:
            buffer.pop(0)
            continue

        frame_len = struct.unpack(">H", bytes(buffer[2:4]))[0]
        if frame_len != 28:
            buffer = []
            continue

        frame = struct.unpack(">HHHHHHHHHHHHHH", bytes(buffer[4:]))

        *values, skip, checksum = frame

        check = sum(buffer[0:30])
        if check != checksum:
            buffer = []
            continue

        sampler.sample(values)

        buffer = buffer[32:]
